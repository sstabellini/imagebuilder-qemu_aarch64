# imagebuilder

This docker image has `mkimage.sh` setup to generate `qemu_aarch64.img` image.

Copy the viryaos-rkt.tar.gz and viryaos-xen.tar.gz tarballs produced by
viryaos-rkt-builder-aarch64 and viryaos-xen-builder-aarch64 to scripts/.

To get started

```
$ cd imagebuilder/scripts/

$ docker build -t image-builder-qemu_aarch64 .
```

Go to the directory containing `ViryaOS` tree.

```
$ docker run --rm --privileged=true -ti -v $(pwd):/home/builder/src -v /dev:/dev -v /tmp:/tmp \
       image-builder-qemu_aarch64 /bin/sh

(as root)
(NOTE: You *cannot* su into `builder` user!)

# apk update

# cd /home/builder/src/imagebuilder/scripts/

/home/builder/src/imagebuilder/scripts # mkdir /tmp/img

/home/builder/src/imagebuilder/scripts # ./mkimage.sh \
  --tag qemu_aarch64 \
  --outdir /tmp/img \
  --arch aarch64 \
  --repository http://dl-cdn.alpinelinux.org/alpine/v3.7/main \
  --extra-repository '@apkrepo-dom0-qemu_aarch64 /home/builder/apkrepo/dom0-qemu_aarch64/v3.7/main' \
  --profile qemu_aarch64
```

The generated disk image is at `/tmp/img/qemu_aarch64.img`.
